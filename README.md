# React Movies
Projet de site web permettant d'avoir des informations exhaustives sur les films.

Intègre des fonctionnalités permettant de gérer les recherches et de filtrer les éléments selon la notation.
Il y a également la possibilité de sauvegarder les films favoris de manière persistant même en rechangeant la page.
Ces informations sont conservées en utilisant le Locale Storage.


### Visualisation du site WEB

https://amk-movies.netlify.app/


### Spécificité du filtre par notation
Deux écouteurs d'événement sont ajoutés sur les boutons pour trier à partir de la meilleurs notation et vice-versa à partir de la mauvaise notation.
Au clique, elles définissent l'état d'une variable appelé `UP` à `true` ou `false`.

En fonction de l'état de cette variable, au niveau de la fonction `sort` qui prend deux arguments,deux résultats diffèrents peuvent être envoyés.

Voici les arguments la fonction sort :  `A=> La moins élévé, B=>La plus élevé`.

Lorsque la variable `up` est définie à true, alors on renvoie : `b.vote_average - a.vote_average` , soit la meilleure notation.
Sinon, c'est `a.vote_average - b.vote_average` qui est renvoyé.
 ```
        <div className="btn-sort-container">
          <div
            className="btn-sort"
            id="goodToBad"
            onClick={(_) => {
              setUp(true);
            }}
          >
            Top <span>→</span>
          </div>
          <div
            className="btn-sort"
            id="badToGood"
            onClick={(_) => {
              setUp(false);
            }}
          >
            Flop <span>→</span>
          </div>
        </div>
      </div>
      <div className="result">
        {moviesData
          .slice(0, 30)
          .sort((a, b) => {
            return up
              ? b.vote_average - a.vote_average
              : a.vote_average - b.vote_average;
          })
          .map((movie, index) => (
            <Cards key={index} movie={movie} genres={genreMovies} />
          ))}
      </div>

```
### Spécificité de corrélation de tableau à objet contenant des tableau


### Spécificité de la gestion du sockage persistant
## Installation

Pour installer les dépendances et lancer  le site dans un environnement de dévellopement :

```bash 
  git clone https://gitlab.com/Ashwin_MK974/react-site-vitrine.git
  cd react-site-vitrine
  npm install 
  npm start
```
    
![Logo](https://miro.medium.com/max/700/1*dLaDL-lSN0iprzmOpmM7zQ.png)

