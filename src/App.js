import React from "react"; //
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import LikePages from "./pages/LikePages";
const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/coup-de-coeur" element={<LikePages />} />
        <Route path="*" element={<Home />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
