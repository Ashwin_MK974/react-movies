//F : 1:32 ; ...[] ? Il s'agit du spread opérateur qui permet de décomposer le tableau
import { React, useEffect, useState } from "react";
import Header from "../components/Header";
import axios from "axios";
import Cards from "../components/Cards";
import genreMovies from "../components/Genre.json";
const LikePages = () => {
  const [listData, setListData] = useState([]);
  const moviesID = window.localStorage.movies?.split(",") || []; //
  useEffect(() => {
    //console.clear();
    //console.log(moviesID);
    for (let i in moviesID) {
      //console.log(moviesID[i]);
      axios
        .get(
          `https://api.themoviedb.org/3/movie/${moviesID[i]}?api_key=${process.env.REACT_APP_API_KEY}&language=en-US`
        )
        .then((res) => {
          //console.log(res);
          setListData((listData) => [...listData, res.data]);
        });
    }
  }, []);
  return (
    <div className="user-list-page">
      <Header />
      <h2>
        Coup de coeur <span>❤️</span>
      </h2>
      <div className="result">
        {listData.length > 0 ? (
          listData.map((movie, index) => (
            <Cards key={index} movie={movie} genres={genreMovies} />
          ))
        ) : (
          <h2>Aucun coup de coeur pour le moment</h2>
        )}
      </div>
    </div>
  );
};

export default LikePages;
