import axios from "axios";
import { React, useEffect, useState } from "react";
import Cards from "./Cards";
const apiKey = process.env.REACT_APP_API_KEY;
console.log("The apiKey =============> " + apiKey);
const Form = () => {
  const [search, setSearch] = useState("code");
  const [moviesData, setMoviesData] = useState([]);
  const [genreMovies, setGenreMovies] = useState();
  const [up, setUp] = useState(true);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${
          search ? search : "code"
        }&language=fr-FR`
      )
      .then((res) => {
        setMoviesData(res.data.results);
        //console.log(moviesData);
      });
    axios
      .get(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=$ {apiKey}&language=en-US`
      )
      .then((res) => {
        //console.log(res.data.genres);
        setGenreMovies(res.data.genres);
      });
  }, [search]);
  return (
    <div className="form-component">
      <div className="form-container">
        <form>
          <input
            type="text"
            name=""
            id="search-input"
            placeholder="Entrer le titre d'un film"
            onChange={(e) => {
              setSearch(e.target.value);
            }}
          />
          <input type="submit" value="Rechercher" />
        </form>
        <div className="btn-sort-container">
          <div
            className="btn-sort"
            id="goodToBad"
            onClick={(_) => {
              setUp(true);
            }}
          >
            Top <span>→</span>
          </div>
          <div
            className="btn-sort"
            id="badToGood"
            onClick={(_) => {
              setUp(false);
            }}
          >
            Flop <span>→</span>
          </div>
        </div>
      </div>
      <div className="result">
        {moviesData
          .slice(0, 30)
          .sort((a, b) => {
            return up
              ? b.vote_average - a.vote_average
              : a.vote_average - b.vote_average;
          })
          .map((movie, index) => (
            <Cards key={index} movie={movie} genres={genreMovies} />
          ))}
      </div>
    </div>
  );
};

export default Form;
