import { React, useEffect, useState } from "react";
import GenreFile from "./Genre.json";
import axios from "axios";
const Cards = (res) => {
  const movie = res.movie;
  //console.log("From Card : ", res.movie);
  const genre = res.genres ? res.genres : GenreFile.genres;
  const statePage = window.location.href.includes("coup-de-coeur");
  //const [genre, setGenre] = useState(GenreFile.genres);
  const dateFormatter = (date) => {
    let [yy, mm, dd] = date.split("-");
    return [dd, mm, yy].join("/");
  };

  const genreFinder = () => {
    let genreArray = [];
    /*     for (let i in movie.genre_ids) {
      for (let x in genre) {
        if (movie.genre_ids[i] == genre[x].id) {
          genreArray.push(genre[x].name);
        }
      }
    } */
    for (let i in genre) {
      /*       console.log(
        `%cID: ${genre[i].id} Genre: ${genre[i].name}`,
        "color:green"
      ); */
      movie.genre_ids.includes(genre[i].id) && genreArray.push(genre[i].name);
    }
    //console.log(genre);
    //console.log(genre[0].id.includes(2));
    //console.log(`%c${genre}`, "color:green");
    //console.log(movie.genre_ids.includes(12));
    for (let i = 0; i < movie.genre_ids.length; i++) {
      //console.log("From Init Film : " + movie.genre_ids[i]);
      for (let x = 0; x < genre.length; x++) {
        //console.log("From direct fetch genre : " + genre[x].id);
        if (movie.genre_ids[i] == genre[x].id) {
          //genreArray.push(genre[x].name);
          //console.log("Genre : " + genre[x].name);
        }
      }
    }
    //console.log(genreArray);
    return genreArray;
  };
  const localStorageManager = () => {
    if (statePage == false) {
      console.log("addd to fav");
      if (window.localStorage.movies == undefined) {
        window.localStorage.movies = movie.id;
      } else {
        window.localStorage.movies.includes(movie.id) != true &&
          (window.localStorage.movies += "," + movie.id);
      }
    } else {
      console.log("delete to fav");
      let storedData = window.localStorage.movies.split(",");
      let newData = storedData.filter((id) => id != movie.id);
      window.localStorage.movies = newData;
      window.location.reload();
    }
  };
  return (
    <div className="card">
      <img
        src={
          movie.poster_path
            ? "https://image.tmdb.org/t/p/original/" + movie.poster_path
            : "./img/poster.jpg"
        }
        alt={`Affiche :  ${movie.title}`}
      />
      <h2>{movie.title}</h2>
      {movie.release_date ? (
        <h5>Sortie le {dateFormatter(movie.release_date)}</h5>
      ) : null}
      <h4>
        {movie.vote_average.toFixed(1)} / 10 <span>⭐</span>
      </h4>
      <ul>
        {movie.genre_ids
          ? genreFinder().map((genre, index) => <li key={index}>{genre}</li>)
          : movie.genres.map((genre, index) => (
              <li key={index}>{genre.name}</li>
            ))}
      </ul>
      {movie.overview && <h3>Synopsis</h3>}
      <p>{movie.overview}</p>
      <div
        className="btn"
        onClick={() => {
          localStorageManager();
        }}
      >
        {(statePage && "Retirer des favoris") || "Ajouter au coups de coeur"}
      </div>
    </div>
  );
};

export default Cards;
